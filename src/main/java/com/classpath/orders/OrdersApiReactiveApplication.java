package com.classpath.orders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersApiReactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdersApiReactiveApplication.class, args);
	}

}

package com.classpath.orders.model;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table("orders")
public class Order {
	
	@Id
    private long orderId;

	@Email(message = "Email address is not valid")
    private String email;
    
	@NotEmpty(message = "customer name cannot be blank")
    private String name;
	
	@PastOrPresent(message = "order data cannot be in the past")
    private LocalDate orderDate;

}

package com.classpath.orders.service;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@Configuration
@RequiredArgsConstructor
public class DomainUserDetailsService implements ReactiveUserDetailsService {
	
	private final UserRepository userRepository;

	@Override
	public Mono<UserDetails> findByUsername(String username) {
		return this.userRepository.findByUsername(username);
	}

	/*
	 * @Override public UserDetails loadUserByUsername(String username) throws
	 * UsernameNotFoundException { return null; }
	 */
	
	
	

}

package com.classpath.orders.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Configuration
@RequiredArgsConstructor
@EnableWebFluxSecurity
@Slf4j
public class ApplicationSecurityConfiguration {
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity httpSecurity) {
		log.info("Inside the security filter chain");
		
		httpSecurity.csrf().disable();
		httpSecurity.cors().disable();
		return httpSecurity
			.authorizeExchange()
			.pathMatchers(HttpMethod.GET, "/api/v1/orders")
				.hasAnyRole("USER", "ADMIN")
			.pathMatchers(HttpMethod.POST, "/api/v1/orders**")
				.hasRole("ADMIN")
			.pathMatchers(HttpMethod.PUT, "/api/v1/orders/**")
				.hasRole("ADMIN")
			.pathMatchers(HttpMethod.DELETE, "/api/v1/orders/**")
				.hasRole("ADMIN")
			.anyExchange()
				.authenticated()
			.and()
				.httpBasic()
			.and()
			.build();
	}
}

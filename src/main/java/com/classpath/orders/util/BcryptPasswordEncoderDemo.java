package com.classpath.orders.util;

import java.util.Scanner;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BcryptPasswordEncoderDemo {
	
	public static void main(String[] args) throws InterruptedException {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		//raw password
		String rawPassword = "welcome";
		
		String encodedPassword1 = passwordEncoder.encode(rawPassword);
		String encodedPassword2 = passwordEncoder.encode(rawPassword);
		String encodedPassword3 = passwordEncoder.encode(rawPassword);
		String encodedPassword4 = passwordEncoder.encode(rawPassword);
		String encodedPassword5 = passwordEncoder.encode(rawPassword);
		
		/*
		 * Flux.just(encodedPassword1, encodedPassword2, encodedPassword3,
		 * encodedPassword4, encodedPassword5) .subscribe(System.out::println);
		 */
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter your password :: ");
		String enteredPassword = scanner.next();
		System.out.println(" Mathces - 1 "+passwordEncoder.matches(enteredPassword, encodedPassword1));
		System.out.println(" Mathces - 2 "+passwordEncoder.matches(enteredPassword, encodedPassword2));
		System.out.println(" Mathces - 3 "+passwordEncoder.matches(enteredPassword, encodedPassword3));
		System.out.println(" Mathces - 4 "+passwordEncoder.matches(enteredPassword, encodedPassword4));
		System.out.println(" Mathces - 5 "+passwordEncoder.matches(enteredPassword, encodedPassword5));
		scanner.close();
		
		//Thread.sleep(Long.MAX_VALUE);
		
	}
	
	

}

package com.classpath.orders.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ServerWebExchange;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Configuration
@Slf4j
@Order(-2)
@RequiredArgsConstructor
public class GlobalExceptionHandler implements ErrorWebExceptionHandler {
	
	private final ObjectMapper objectMapper;
	
	@Override
	public Mono<Void> handle(ServerWebExchange serverExchange, Throwable exception) {
		log.error("Came inside the catch block " + exception);
		DataBufferFactory bufferFactory = serverExchange.getResponse().bufferFactory();
		DataBuffer dataBuffer = null;
		if (exception instanceof IllegalArgumentException) {
			serverExchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
			serverExchange.getResponse().setStatusCode(HttpStatus.NOT_FOUND);
		}else if (exception instanceof 	WebExchangeBindException) {
			serverExchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
			serverExchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);
			WebExchangeBindException bindException = (WebExchangeBindException)exception;
			List<ObjectError> allErrors = bindException.getAllErrors();
			List<String> errors = allErrors.stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
			try {
				dataBuffer = bufferFactory.wrap(objectMapper.writeValueAsBytes(errors));
				return serverExchange.getResponse().writeWith(Mono.just(dataBuffer));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		try {
			dataBuffer = bufferFactory.wrap(objectMapper.writeValueAsBytes(new Error (100, exception.getMessage())));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return serverExchange.getResponse().writeWith(Mono.just(dataBuffer));
	}
}

@AllArgsConstructor
@Getter
class Error {
	private final int code;
	private final String message;
}

package com.classpath.orders.controller;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.classpath.orders.model.Todo;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/todos")
@RequiredArgsConstructor
public class TodoRestController {
	
	private final RestTemplate restTemplate;
	private final WebClient webClient;
	private final String TODO_URL="https://jsonplaceholder.typicode.com/todos";
	
	@GetMapping
	public String todos() {
		
		//blocking operation
		ResponseEntity<String> response = this.restTemplate.getForEntity(TODO_URL, String.class);
		return response.getBody();
	}
	
	@GetMapping("/reactive")
	public List<Todo> todosUsingWebClient() {
		
		/*
		 * String todos = this.webClient .get() .retrieve() .bodyToMono(String.class)
		 * .block();
		 */
		
		List<Todo> todos = this.webClient
			.get()
			.retrieve()
			.bodyToMono(new ParameterizedTypeReference<List<Todo>>() {})
			//blocking operation
			.block();
		
		
		return todos;
	}

}
